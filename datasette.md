**Datasette to use locally**

Download csv files:

1. [FilmLocations.csv](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DB0201EN-SkillsNetwork/labs/Labs_Coursera_V5/datasets/FilmLocations.csv)

2. [Instructor.csv](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DB0201EN-SkillsNetwork/labs/Labs_Coursera_V5/datasets/Instructor.csv)

Install first anaconda distribution using the link given below:

[https://www.anaconda.com/products/individual](https://www.anaconda.com/products/individual)

As you scroll down, you will get the installers for Windows, Mac and Linux.

Now, after the installation completes. **Open Anaconda Navigator using run as administrator** and it will look as:

![image](.images/datasette1.png)

Click &#39;Launch&#39; for JupyterLab. This will open the Jupyter Lab in local host in browser.

Click on File \&gt; New \&gt; Terminal

![image](.images/datasette2.png)

The window looks as:

![image](.images/datasette3.png)

Use the below commands one by one to start the datasette

1. sqlite3
2. .mode csv
3. .import provide_path_csv filename

For example, if the location is something as C:\Users\Skill07\Downloads\FilmLocations.csv so you have to write it as:

**.import C:/Users/Skill07/Downloads/FilmLocations.csv FilmLocations** 

1. .save filename.db

For example, above I have used FilmLocations as a filename so save command will be used as:

**.save FilmLocations.db**

**Now, your .csv file is converted to .db using sqlite**

1. **.exit (We now exit the sqlite using exit command)**
2. **Use the below command to install datasette as:**

**pip install datasette**

1. **To launch datasette, use the below command:**

**datasette filename.db**

**as per above example, this will be written as : datasette FilmLocations.db**

![image](.images/datasette4.png)

**When the datasette server started, you will get the link as highlighted below:**

![image](.images/datasette5.png)

**Remove 1m or any keyword that comes before http:// and use in your browser. So, I will use** [**http://127.0.0.1:8001**](http://127.0.0.1:8001/)

**I have my dataset now in my server**

![image](.images/datasette6.png)

**Click on FilmLocations hyperlink to work on dataset.**

**Click on View and edit SQL**

![image](.images/datasette7.png)

**You can write your SQL queries as:**

![image](.images/datasette8.png)

**You can now execute the queries that are given in the instructions.**

**NOTE:**

1. **If there is any column having space in columns, you need to use that in square braces**

**like [Release Year]**

2. **In locally we cannot use create and updated queries in Datasette. If you want to try insert and update commands, upload the csv files in IBM Cloud db2 ( To upload the files follow the instruction given in the [link](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DB0201EN-SkillsNetwork/labs/Labs_Coursera_V5/labs/Lab%20-%20Create%20tables%20using%20SQL%20scripts%20and%20Load%20data%20into%20tables/instructional-labs.md.html)) and use the queries.**